package com.userservice.UserService.util;

import com.userservice.UserService.dto.UserDTO;

public class Validations {
    public static boolean validateUser(UserDTO user) {
        if (user.getName() == null || user.getAge() == null) {
            return false;
        } else return Integer.parseInt(user.getAge()) <= 120 && Integer.parseInt(user.getAge()) >= 0;
    }
}
